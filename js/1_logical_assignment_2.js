var result1 = undefined && 42;
console.log (result1);

var result2 = 0 && ["Sweet", "array"];
console.log (result2);

var result3 = "" &&  {type: "ring", stone: "diamond"};
console.log(result3);

var result1 = "King"  && "Arthur" ;
console.log(result1);
var result2 = "Arthur"  && "King" ;
console.log(result2);

var result1 = "King"  || "Arthur" ;
console.log(result1);
var result2 = "Arthur"  || "King" ;
console.log(result2);

var result1 = undefined && "" ;
console.log(result1);
var result2 = ""  && undefined ;
console.log(result2);

var result1 = undefined || "" ;
console.log(result1);
var result2 = ""  || undefined ;
console.log(result2);

armory = {
    addSword : function(sword){
        this.swords = this.swords || [];
        this.swords.push(sword);
    }
};
armory.addSword("Broadsword");
armory.addSword("Katana");
armory.addSword("Claymore");
armory.addSword("Scimitar");
armory.retrieveSword = function (request)
    {
        return (this.swords.indexOf(request) >= 0) ?
            this.swords.splice(this.swords.indexOf(request), 1)[0] :
            alert("No " + request + ", baby!");        
    };
var isKnight = true;    
var weapon = isKnight && armory.retrieveSword("Katana");
console.log(weapon);
var weapon = isKnight && armory.retrieveSword("Rapier");
console.log(weapon);
var isKnight = false;
var weapon = isKnight && armory.retrieveSword("Rapier");
console.log(weapon);
var armoryIsOpen = true;
var isKnight = true;
var weapon = armoryIsOpen && isKnight && 
        armory.retrieveSword("Claymore");
console.log(weapon);
var armoryIsOpen = false;
var weapon = armoryIsOpen && isKnight && 
        armory.retrieveSword("Claymore");
console.log(weapon);