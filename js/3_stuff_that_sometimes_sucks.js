var drawbridge = 
    {
        soldiers: 8,
        capacity: 20,
        open: function()
            {
                alert ("rrrrrrrrrrBANG!");
            }
    };
with (drawbridge) //the object is instances as a new global variable
                  //so while we can call functions, we can't assign methods to it  
    {
        open();
    }
var castle = 
    {
        soldiers: 865,
        capacity: 3000,
        keep:
            {
                soldiers: 19,
                capcity: 40,
                drawbridge : 
                    {
                        soldiers: 8,
                        capacity: 20,
                        open: function()
                            {
                                alert ("rrrrrrrrrrBANG!");
                            },
                        close: function()
                            {
                                alert ("yunyunyunyunyunCLACK!");
                            }    
                    }
            }
    };
var reinforcements = 12;
//Instead of with, that makes unclear what we are modifying, we can
//use a local variable, as seen previously
var o = castle.keep.drawbridge;
if (o.capacity >= o.soldiers + o.reinforcements)
    {
        o.open();
        o.soldiers += o.reinforcements;
        alert ("Drawbridge soldiers "+o.soldiers);
        close();
    }
else
    {
        alert ("Reinforcements would requiere split unit");
    }
//if motto contains (') for example, the code gives error    
function assignRegimentMotto (number, motto)
    {
        eval("regiment"+number+".motto = '" + motto +"'");
    }
//so we reduce eval as much as possible
function assignRegimentMotto (number, motto)
    {
        eval("regiment"+number).motto = motto;
    }
//however, in this case, eval can be avioded completely if the regiments are on an array
regiments = [ /*list of regiments*/];
function assignRegimentMotto (number, motto)
    {
        regiment[number].motto = motto;
    }
var regimentsJSON = "{regiment_1: {motto: 'The Kings Own'," + 
                    'numMembers: 46,' + 
                    'captain: "Jar Treen"}'+
                    //...
                    "}";
eval (regimentsJSON);   
//Since JSON usually comes from somewhere else, like a form,
//this code is vulnerable to Code Injections
//So instead, we can use JSON.parse() to ensure that only JSON is accepted
JSON.parse(regimentsJSON);
var isKing = false,
    weapon;
if (isKing) //While no brackets are necessary now, if you want to add extra code
            //you or whoever has to add the extra code has to analyze the code to
            //see where you add the brackets anyways.
    weapon = "Excalibur";
else
    weapon = "Longsword";

//Without the else statement, the code may work, but the entire code won't be conditioned to the IF
if (isKing)
    weapon = "Excalibur";
    alert ("All Hail Arthur, King of the Britons!"); //Executes no matter what