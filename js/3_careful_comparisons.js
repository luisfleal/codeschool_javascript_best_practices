function countEntries (KnightResponses, value)
    {
        var count = 0, x = KnightResponses.length;
        for (var i = 0; i < x; i++)
            {
                if (KnightResponses[i] === value)
                    {
                        count++;
                    }
            }
        return count;    
    }
var fields = ["Jason Millhouse", "1", "12", true, true, false];
var numCompletedTasks = countEntries (fields, true);
console.log (numCompletedTasks);

function Armor (location)
    {
        this.location = location;
    };
Armor.prototype = 
    {
        putOn: function ()
            {
                alert ("Your armor is on!");
            }
    };

function LeatherArmor (bodyStyle, numBuckles, numSpaulders)
    {
        this.bodyStyle = bodyStyle;
        this.numBuckles = numBuckles;
        this.numSpaulders = numSpaulders;
    }
LeatherArmor.prototype = Object.create (Armor.prototype);    
function ChainMail (metal, linkDiameter, hasHood, skirtLength)
    {
        this.metal = metal;
        this.linkDiameter = linkDiameter;
        this.hasHood = hasHood;
        this.skirtLength = skirtLength;
    }
ChainMail.prototype = Object.create (Armor.prototype); 
//We fill our newbs list with Knight Objects
//And our armorLists with armor objects
var newbList = ["Grimble Horsehead", "Jack Winterborn", "Bunder Ropefist",
                            "Ernst Breadbaker"];
var newbs = [];
for (var i = 0, x = newbList.length; i < x; i++){
    newbs.push (new Knight (newbList[i], 1));
};

var armorList = [];
for (var i = 0, x = 9; i < x; i++){
    switch (i % 2)
        {
            case 0:
                armorList.push (new LeatherArmor ("Medium", 4, 36));
            break;
            default:
                armorList.push (new ChainMail ("Iron", 3.5, true, 36));
        }
};
function assignKnightsArmor ( knights, armorAvail )
    {
        var x = knights.length;
        var y = armorAvail.length;
        for (var i = 0; i < x ; i++)
            {
                for (var j = 0; j < y; j++)
                    {
                        if (armorAvail[j] instanceof ChainMail)
                            {
                                knights[i].armor = armorAvail.splice(j, 1)[0];
                                y--;
                                break;
                            }
                    }
            }
    };
assignKnightsArmor( newbs, armorList );
console.log (armorList);
console.log (newbs[0]);

var kingsMail = new ChainMail ("gold", 2, true, 36);
console.log (kingsMail instanceof Armor);