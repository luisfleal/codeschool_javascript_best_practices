console.log (0.1 + 0.2);
console.log (0.1 + 0.2 + 0.3);
console.log ((0.1 + 0.2) + 0.3);
console.log (0.1 + (0.2 + 0.3));
var num = 0.1 + 0.2;
console.log(num.toFixed(1));
num = 0.1 + 0.2 + 0.3;
console.log(num.toFixed(4));
function tax (price, percent)
    {
        return parseFloat((price*percent/100).toFixed(2));
    }
console.log(tax(9.85, 7.5));//toFixed returns a string so the parseFloat method is needed
//to be able to operate with the result

var mailedGlove = 9.85;
var armorTax = 7.5;
var total = mailedGlove + tax (mailedGlove, armorTax);
console.log(total);
console.log(parseInt("88"));
console.log(parseInt("88 keys in a piano"));
console.log(parseFloat("3.28084 meters in a foot"));
console.log(parseInt("There are 88 keys in a piano"));
console.log(parseInt("9.85"));
var userAge = "021";

console.log(parseInt(userAge));
console.log(parseInt(userAge, 10));

console.log (isNaN("42"));
function isThisActuallyANumberDontLie (data)
    {
        return (typeof data === "number" && !isNaN(data));
    }
console.log(isThisActuallyANumberDontLie(640));
console.log(isThisActuallyANumberDontLie("640"));
console.log(isThisActuallyANumberDontLie(NaN));

function checkValidZip ()
    {
        var entry = document.getElementById("zip").value;
        var userZip = parseInt(entry);
        try
            {
                if (isThisActuallyANumberDontLie(userZip))
                    {
                        if (userZip.toFixed(0).length === 5)
                            {
                                return true;
                            } 
                            else
                                {
                                    throw new Error ("Nope!");
                                }
                    }
                    else
                        {
                            throw new Error ("Nope!");
                        }
            }catch (e)
                {
                    if (e.message = "Nope!")
                        {
                            alert ("Please enter a valid zip code, dude!");
                            return false;
                        }
                    //other error responses go here    
                }
    }
checkValidZip();    