var armory = {
    addSword : function(sword){
        this.swords = this.swords ? this.swords: [];
        this.swords.push(sword);
    }
};

armory = {
    addSword : function(sword){
        this.swords = this.swords || [];
        this.swords.push(sword);
    }
};
armory.addSword("Broadsword");
armory.addSword("Katana");
armory.addSword("Claymore");
armory.addSword("Scimitar");
console.log(armory.swords);

//Since the OR operator short-circuits to the left-most TRUE value, in this case, 
//it always returns an empty
//list and then assigns the last operator added to it
armory = {
    addSword : function(sword){
        this.swords = [] || this.swords;
        this.swords.push(sword);
    }
};
armory.addSword("Broadsword");
armory.addSword("Katana");
armory.addSword("Claymore");
armory.addSword("Scimitar");
console.log(armory.swords);

var result1 = 42 || undefined;
console.log(result1);
var result2 = ["Sweet", "array"] || 0;
console.log(result2);
var result3 = {type: "ring", stone: "diamond"} || "";
console.log(result3);

var result1 = undefined  || 42 ;
console.log(result1);
var result2 = 0 ||  ["Sweet", "array"];
console.log(result2);
var result3 = "" ||  {type: "ring", stone: "diamond"};
console.log(result3);

var result1 = "King"  || "Arthur" ;
console.log(result1);
var result2 = "Arthur"  || "King" ;
console.log(result2);

var result1 = undefined || "" ;
console.log(result1);
var result2 = ""  || undefined ;
console.log(result2);