var rightNow = +new Date();
var endTime = +new Date();
var elapsedTime = endTime - rightNow;
console.log (elapsedTime);

function SpeedTest (testImplement, testParams, repetitions)
    {
        this.testImplement = testImplement;
        this.testParams = testParams;
        this.repetitions = repetitions || 10000;
        this.average = 0;
    };
SpeedTest.prototype = 
    {
        startTest : function () 
            {
                var beginTime, endTime, sumTimes = 0;
                for (var i = 0, x = this.repetitions; i < x ; i++)
                    {
                        beginTime = +new Date();
                        this.testImplement ( this.testParams );
                        endTime = +new Date();
                        sumTimes += endTime - beginTime;
                    }
                 this.average = sumTimes / this.repetitions;
                 return console.log ("Average execution across: " + 
                                     this.repetitions + ": " +
                                     this.average);
            }
    };
function Knight (name, regiment)
{
    this.name = name;
    this.regiment = regiment;
    switch (regiment){
        case 1:
            this.weapon = "Broadsword";
            break;
        case 2:
            this.weapon = "Claymore";
            break;
        case 3:
            this.weapon = "Longsword";
            break;
        
        case 5:
            this.weapon = "War Hammer";
            break;
        case 6:
            this.weapon = "Battle Axe";
            break;
        case 4:    
        case 7:
        case 8:
            this.weapon = "Morning Star";
            break;
        case "King":
            this.weapon = "Excalibur";
            break;
        default:
            alert (name + " has an incorrect regiment, Master Armourer!" + 
                    "\n\nNo weapon assigned!");
            break;
    }
};


var firstRegimentNewbs = ["Grimble Horsehead", "Jack Winterborn", "Bunder Ropefist",
                            "Ernst Breadbaker"];
var firstRegimentKnights = [];
var listForTests = [firstRegimentNewbs, firstRegimentKnights];
var noBP = function ( listOfParams )
{
    for (var i = 0; i < listOfParams[0].length ; i++){
        var newGuy = new Knight (listOfParams[0][i], 1);
        listOfParams[1].push (newGuy);
    };
};

var noBPTest = new SpeedTest (noBP, listForTests, 100000);
noBPTest.startTest();

var BP = function ( listOfParams )
{
    for (var i = 0, x = listOfParams[0].length; i < x ; i++){
        listOfParams[1].push (new Knight (listOfParams[0][i], 1));
    };
};

var BPTest = new SpeedTest (noBP, listForTests, 100000);
BPTest.startTest();