function Knight (name, regiment)
{
    this.name = name;
    this.regiment = regiment;
    switch (regiment){
        case 1:
            this.weapon = "Broadsword";
            break;
        case 2:
            this.weapon = "Claymore";
            break;
        case 3:
            this.weapon = "Longsword";
            break;
        
        case 5:
            this.weapon = "War Hammer";
            break;
        case 6:
            this.weapon = "Battle Axe";
            break;
        case 4:    
        case 7:
        case 8:
            this.weapon = "Morning Star";
            break;
        case "King":
            this.weapon = "Excalibur";
            break;
        default:
            alert (name + " has an incorrect regiment, Master Armourer!" + 
                    "\n\nNo weapon assigned!");
            break;
    }
};

var firstRegimentNewbs = ["Grimble Horsehead", "Jack Winterborn", "Bunder Ropefist",
                            "Ernst Breadbaker"];
var firstRegimentKnights = [];
var secondRegimentNewbs = ["Jenner Pond", "Tar Backstrand", "Cromer Treen", "Stim Lancetip",
                            "Vorn Sharpeye", "Rack Leaflets", "Brck Valleyhome", "Arden Follower"];                       
var secondRegimentKnights = [];
console.time ("Total completion time");
//console.time ("Time to add " + firstRegimentNewbs.length + " Knights!");
for (var i = 0, x = firstRegimentNewbs.length; i < x; i++){
    firstRegimentKnights.push (new Knight (firstRegimentNewbs[i], 1));
};
//both time and timeEnd must have the exact same string to match
//console.timeEnd ("Time to add " + firstRegimentNewbs.length + " Knights!");
//console.time ("Time to add " + secondRegimentNewbs.length + " Knights!");
for (var i = 0, x = secondRegimentNewbs.length; i < x; i++){
    secondRegimentKnights.push (new Knight (secondRegimentNewbs[i], 1));
};
//both time and timeEnd must have the exact same string to match
//console.timeEnd ("Time to add " + secondRegimentNewbs.length + " Knights!");
console.timeEnd ("Total completion time");