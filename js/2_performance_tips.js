/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function SignalFire (ID, startingLogs){
    this.fireID = ID;
    this.logsLeft = startingLogs;
    this.addLogs = function(numLogs)
    {
        //...
    };
    this.lightFire = function()
    {
        //...
    };
    this.smokeSignal = function (message)
    {
        //...
    };
}

//Inheritance version:
function SignalFire (ID, startingLogs)
    {
        this.fireID = ID;
        this.logsLeft = startingLogs;
    };
SignalFire.prototype = 
    {
        addLogs : function(numLogs)
            {
                //...
            },
        lightFire : function()
            {
                //...
            },
        smokeSignal : function (message)
            {
                //...
            }
};

var fireOne = new SignalFire (1, 20);
var fireTwo = new SignalFire (2, 18);
var fireThree = new SignalFire (3, 24);

fireOne.addLogs(8);
fireTwo.addLogs(10);
fireThree.addLogs(4);

//This is more efficient since each SignalFire object doesn't contain all methods, they access them
//through the Prototype.

//With a DOM fragment, we can create the entire list then append it
//all at once
//Also, you can have a list of variables behind a single var declaration
//since each var adds a look-up to the JS parser
var list = document.getElementById("kotwList"),
    kotw = ["Jenna Rangespike",
            "Neric Farthing",
            "Darkin Stonefield"],
    fragment = document.createDocumentFragment(),
    element;
for (var i=0, x = kotw.length; i < x; i++)
{
    element = document.createElement("li");
    element.appendChild ( document.createTextNode (kotw [i]) );
    fragment.appendChild (element);
}
list.appendChild (fragment);

//String concatenation
var knight = "Jenna Rangespike",
    action = " strikes the enemy with a ",
    weapon = "halberd",
    turn = "";
turn += knight;
turn += action;
turn += weapon;
//The standard concatenator operator is ideal for small number of strings.

var newPageBuild =["<!DOCTYPE html>", "<html>", "<body>", "<h1>",
                   //a hundred other html elements
                   "</h1>", "</body>", "</html>"],
    page = "";
page = newPageBuild.join("\n"); 
console.log(page);
//Array.join is more efficient than looping though each element of the array to concatenate them
//also is easier to read