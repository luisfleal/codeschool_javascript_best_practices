treasureChest = {
    goldCoins : 10000,
    magicalItem : "Crown of Speed",
    necklaces : ['ruby', 'pearl', 'sapphire', 'diamond'],
    openLid : function (){
        alert ("Creeeeak!");
    }
};
console.log("You've found the following necklaces!");
for (var i = 0; i < treasureChest.necklaces.length; i++)
    {
        console.log(treasureChest.necklaces[i]);
    };
//Using a cached value in a local variable will avoid having to access the length value each time
console.log("You've found the following necklaces!");
var x = treasureChest.necklaces.length;
for (var i = 0; i < x; i++)
    {
        console.log(treasureChest.necklaces[i]);
    };
for (var i = 0, x = treasureChest.necklaces.length; i < x; i++)
    {
        console.log(treasureChest.necklaces[i]);
    };
var list = treasureChest.necklaces;
for (var i = 0, x = treasureChest.necklaces.length; i < x; i++)
    {
        console.log(list[i]);
    };       